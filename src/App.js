import './App.css';
import Dashboard from './pages/Dashboard/Dashboard';

function App() {
  return (
    <div className="App" id="app">
      <Dashboard />
    </div>
  );
}

export default App;
