import React from 'react';

export default function CountryTable(props) {
    const { countryList, searchTerm } = props;

    // GET COUNTRY ID
    const getCountryId = (data) => {
        return data.split('-')[0].toLowerCase();
    }

    return (
        <>
            <table className="myTable">
                <tbody>
                    <tr className="border">
                        <th className="headerTitle">#</th>
                        <th className="headerTitle">Name</th>
                        <th className="headerTitle">Country</th>
                    </tr>
                    {countryList?.map((data, index) => (
                        <tr key={index} className="border">
                            <th className="table-data" scope="row">{index + 1}</th>
                            <td className="table-data">{data.PlaceName}</td>
                            <td className="table-data"> <span>{data.CountryName}</span> <img src={`https://www.countryflags.io/${getCountryId(data.CountryId)}/flat/16.png`} /></td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {searchTerm && countryList.length === 0 &&
                <div className="message">No result found</div>
            }
            {!searchTerm && countryList.length === 0 &&
                <div className="message">Start searching</div>
            }
        </>
    );
}
