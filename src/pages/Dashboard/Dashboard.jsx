// CORE 
import axios from 'axios';
import React, { useEffect, useRef, useState } from 'react';

// CUSTOM COMPONENT
import CountryTable from './component/CountryTable';

const Dashboard = () => {
    const inputRef = useRef();
    const [countryList, setCountryList] = useState([]);
    const [searchTerm, setSearchTerm] = useState();
    const [loading, setLoading] = useState();

    // FOCUS ON INPUT IF CTRL AND / KEY PRESSED
    useEffect(() => {
        addEvent(document, "keypress", function (e) {
            e = e || window.event;

            if (e.ctrlKey && e.key === '/')
                inputRef.current.focus();
        });

        function addEvent(element, eventName, callback) {
            if (element.addEventListener)
                element.addEventListener(eventName, callback, false);
            else if (element.attachEvent)
                element.attachEvent("on" + eventName, callback);
            else
                element["on" + eventName] = callback;
        }
    }, [])

    // HANDLE SEARCH EVENT 
    const doSearch = (event) => {
        if (event.key === 'Enter') {
            setSearchTerm(event.target.value)
            if (event.target.value)
                searchAPICalling(event.target.value);
            else
                setCountryList([])
        }
    }

    // API CALLING TO SEARCH DATA
    const searchAPICalling = (queryParams) => {
        setLoading(true)
        axios.defaults.headers.common['x-rapidapi-key'] = process.env.REACT_APP_RAPIDAPI_KEY
        axios.defaults.headers.common['x-rapidapi-host'] = process.env.REACT_APP_RAPIDAPI_HOST;
        axios.get(`${process.env.REACT_APP_API_URL}?query=${queryParams}`).then((data) => {
            if (data.status === 200)
                setCountryList(data.data.Places)
            
            setLoading(false)
        });
    }

    return (
        <div className="continaerDiv">
            <input ref={inputRef} onKeyDown={doSearch} className="inputBox" />
            {loading &&
                <div className="loader"></div>
            }
            <CountryTable countryList={countryList} searchTerm={searchTerm} />
        </div>
    )
}

export default Dashboard;